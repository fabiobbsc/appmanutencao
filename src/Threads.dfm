object fThreads: TfThreads
  Left = 0
  Top = 0
  Caption = 'Teste Threads'
  ClientHeight = 322
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  DesignSize = (
    630
    322)
  PixelsPerInch = 96
  TextHeight = 13
  object LabelThreads: TLabel
    Left = 10
    Top = 7
    Width = 98
    Height = 13
    Caption = 'N'#250'mero de Threads:'
  end
  object Label1: TLabel
    Left = 137
    Top = 7
    Width = 83
    Height = 13
    Caption = 'Tempo de Espera'
  end
  object Edit1: TEdit
    Left = 10
    Top = 26
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '10'
  end
  object Edit2: TEdit
    Left = 137
    Top = 26
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '500'
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 305
    Width = 630
    Height = 17
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 291
    ExplicitWidth = 617
  end
  object ButtonExecutar: TButton
    Left = 276
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Executar'
    TabOrder = 3
    OnClick = ButtonExecutarClick
  end
  object Memo1: TMemo
    Left = 10
    Top = 55
    Width = 612
    Height = 244
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 4
    ExplicitWidth = 599
    ExplicitHeight = 230
  end
end
