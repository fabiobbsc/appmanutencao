unit ClienteServidor;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  Datasnap.DBClient,
  Data.DB;

type
  TServidor = class
  private
    FPath: String;
  public
    constructor Create;
    // Tipo do par�metro n�o pode ser alterado
    procedure DeletarArquivos(APosicao: Integer);
    function SalvarArquivos(AData: OleVariant; AIncremento: Integer = 0): Boolean;
  end;

  TfClienteServidor = class(TForm)
    ProgressBar: TProgressBar;
    btEnviarSemErros: TButton;
    btEnviarComErros: TButton;
    btEnviarParalelo: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btEnviarSemErrosClick(Sender: TObject);
    procedure btEnviarComErrosClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btEnviarParaleloClick(Sender: TObject);
  private
    FPath: String;
    FServidor: TServidor;

    function InitDataset: TClientDataset;
  public
  end;

var
  fClienteServidor: TfClienteServidor;

const
  QTD_ARQUIVOS_ENVIAR = 100;

implementation

uses
  IOUtils,
  System.SyncObjs;

{$R *.dfm}

procedure TfClienteServidor.btEnviarComErrosClick(Sender: TObject);
var
  cds: TClientDataset;
  i: Integer;
begin
  try
    Self.ProgressBar.Max := QTD_ARQUIVOS_ENVIAR + 1;
    Self.ProgressBar.Position := 0;

    for i := 0 to QTD_ARQUIVOS_ENVIAR do
    begin
      Self.ProgressBar.Position := Self.ProgressBar.Position + 1;
      Self.ProgressBar.Update;

      Application.ProcessMessages();

      cds := InitDataset;

      cds.Append;
      TBlobField(cds.FieldByName('Arquivo')).LoadFromFile(FPath);
      cds.Post;

      FServidor.SalvarArquivos(cds.Data, i);

      if Assigned(cds) then
      begin
        FreeAndNil(cds);
      end;

      try
{$REGION Simula��o de erro, n�o alterar}
        if i = (QTD_ARQUIVOS_ENVIAR / 2) then
          FServidor.SalvarArquivos(NULL);
{$ENDREGION}
      except
        on E: Exception do
        begin
          FServidor.DeletarArquivos(i);
          ShowMessage('Erro ao gerar arquivos!' + sLineBreak + 'Todos os arquivos gerados at� o momento ser�o exclu�dos.' + sLineBreak + sLineBreak + 'Erro original: ' + E.Message);
          Break;
        end;
      end;
    end;
  finally
    Self.ProgressBar.Position := 0;
    Self.ProgressBar.Update;
  end;

  Self.btEnviarSemErros.Click();
end;

procedure TfClienteServidor.btEnviarParaleloClick(Sender: TObject);
var
  i: Integer;
  sCaption: String;

  procedure EnviarArquivo(APosicao: Integer);
  begin
    TThread.CreateAnonymousThread(
      procedure
      var
        cds: TClientDataset;
      begin
        TThread.Synchronize(TThread.CurrentThread,
          procedure
          begin
            Self.Caption := 'Enviando, aguarde...';
            Self.Update();

            cds := InitDataset;

            cds.Append;
            TBlobField(cds.FieldByName('Arquivo')).LoadFromFile(Self.FPath);
            cds.Post;

            FServidor.SalvarArquivos(cds.Data, APosicao);

            if Assigned(cds) then
            begin
              FreeAndNil(cds);
            end;

            Self.ProgressBar.Position := Self.ProgressBar.Position + 1;
            Self.ProgressBar.Update;
          end);
      end).Start();
  end;

begin
  try
    sCaption := Self.Caption;
    Self.Caption := 'Aguarde, preparando envio...';
    Self.Update();

    Self.ProgressBar.Max := QTD_ARQUIVOS_ENVIAR + 1;

    Self.ProgressBar.Position := 0;

    for i := 0 to QTD_ARQUIVOS_ENVIAR do
    begin
      EnviarArquivo(i);
    end;

    ShowMessage('Envio conclu�do!');
  finally
    Self.Caption := sCaption;
    Self.Update();
    Self.ProgressBar.Position := 0;
    Self.ProgressBar.Update;
  end;
end;

procedure TfClienteServidor.btEnviarSemErrosClick(Sender: TObject);
var
  cds: TClientDataset;
  i: Integer;
begin
  try
    Self.ProgressBar.Max := QTD_ARQUIVOS_ENVIAR + 1;
    Self.ProgressBar.Position := 0;

    for i := 0 to QTD_ARQUIVOS_ENVIAR do
    begin
      Self.ProgressBar.Position := Self.ProgressBar.Position + 1;
      Self.ProgressBar.Update;

      // Application.ProcessMessages();

      cds := InitDataset;

      cds.Append;
      TBlobField(cds.FieldByName('Arquivo')).LoadFromFile(Self.FPath);
      cds.Post;

      FServidor.SalvarArquivos(cds.Data, i);

      if Assigned(cds) then
      begin
        FreeAndNil(cds);
      end;
    end;

    ShowMessage('Envio conclu�do!');
  finally
    Self.ProgressBar.Position := 0;
    Self.ProgressBar.Update;
  end;
end;

procedure TfClienteServidor.FormCreate(Sender: TObject);
begin
  inherited;
{$WARN SYMBOL_PLATFORM OFF}
  FPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'pdf.pdf';
{$WARN SYMBOL_PLATFORM ON}
  FServidor := TServidor.Create;
end;

procedure TfClienteServidor.FormDestroy(Sender: TObject);
begin
  if Assigned(Self.FServidor) then
  begin
    FreeAndNil(Self.FServidor);
  end;
end;

function TfClienteServidor.InitDataset: TClientDataset;
begin
  Result := TClientDataset.Create(nil);
  Result.FieldDefs.Add('Arquivo', ftBlob);
  Result.CreateDataSet;
end;

{ TServidor }

constructor TServidor.Create;
begin
  FPath := ExtractFilePath(ParamStr(0)) + 'Servidor\';

  if not DirectoryExists(Self.FPath) then
  begin
    CreateDir(Self.FPath);
  end;
end;

function TServidor.SalvarArquivos(AData: OleVariant; AIncremento: Integer = 0): Boolean;
var
  cds: TClientDataset;
  FileName: string;
begin
  Result := False;

  try
    try
      cds := TClientDataset.Create(nil);
      cds.Data := AData;

{$REGION Simula��o de erro, n�o alterar}
      if cds.RecordCount = 0 then
        Exit;
{$ENDREGION}
      cds.First;

      while not cds.Eof do
      begin
        FileName := FPath + (cds.RecNo + AIncremento).ToString + '.pdf';
        if TFile.Exists(FileName) then
          TFile.Delete(FileName);

        TBlobField(cds.FieldByName('Arquivo')).SaveToFile(FileName);
        cds.Next;
      end;

      Result := True;
    except
      raise;
    end;
  finally
    if Assigned(cds) then
    begin
      FreeAndNil(cds);
    end;
  end;
end;

procedure TServidor.DeletarArquivos(APosicao: Integer);
var
  i: Integer;
  FileName: String;
begin
  for i := 0 to APosicao do
  begin
    FileName := FPath + (i + 1).ToString + '.pdf';

    if TFile.Exists(FileName) then
    begin
      TFile.Delete(FileName);
    end;
  end;
end;

end.
