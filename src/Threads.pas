unit Threads;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.StdCtrls;

type
  TfThreads = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    ProgressBar1: TProgressBar;
    ButtonExecutar: TButton;
    Memo1: TMemo;
    LabelThreads: TLabel;
    Label1: TLabel;
    procedure ButtonExecutarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  public
    { Public declarations }
  end;

var
  fThreads: TfThreads;

implementation

{$R *.dfm}

procedure TfThreads.ButtonExecutarClick(Sender: TObject);

  procedure Executar(AId: Integer; ATime: Integer);
  var
    oThread: TThread;
  begin
    oThread := TThread.CreateAnonymousThread(
      procedure
      var
        i: Integer;
      begin
        TThread.Synchronize(TThread.CurrentThread,
          procedure
          begin
            Memo1.Lines.Add(AId.ToString() + ' - Iniciando processamento');
          end);

        for i := 0 to 100 do
        begin
          TThread.Sleep(Random(ATime));

          TThread.Synchronize(TThread.CurrentThread,
            procedure
            begin
              Self.ProgressBar1.Position := Self.ProgressBar1.Position + 1;
              Application.ProcessMessages();
            end);
        end;

        TThread.Synchronize(TThread.CurrentThread,
          procedure
          begin
            Memo1.Lines.Add(AId.ToString() + ' - Processamento finalizado');

            if Self.ProgressBar1.Position = Self.ProgressBar1.Max then
            begin
              Self.ButtonExecutar.Enabled := true;
            end;
          end);
      end);

    oThread.FreeOnTerminate := true;
    oThread.Start();
  end;

var
  i: Integer;
begin
  Self.ProgressBar1.Max := 101 * String(Self.Edit1.Text).ToInteger();
  Self.ProgressBar1.Position := 0;
  Self.ButtonExecutar.Enabled := False;
  Self.Memo1.Lines.Clear();

  for i := 1 to String(Self.Edit1.Text).ToInteger() do
  begin
    Executar(i, String(Self.Edit2.Text).ToInteger());
  end;
end;

procedure TfThreads.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  while not Self.ButtonExecutar.Enabled do
  begin
    Self.Caption := 'Aguardando t�rmino...';
    Application.ProcessMessages();
  end;
end;

end.
