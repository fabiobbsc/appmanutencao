unit Main;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  System.SyncObjs;

type
  TfMain = class(TForm)
    btDatasetLoop: TButton;
    btThreads: TButton;
    btStreams: TButton;
    procedure btDatasetLoopClick(Sender: TObject);
    procedure btStreamsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btThreadsClick(Sender: TObject);
  private
    FRegistroLog: TCriticalSection;

    procedure OnCustomException(Sender: TObject; E: Exception);
  public
  end;

var
  fMain: TfMain;

implementation

uses
  DatasetLoop,
  ClienteServidor, Threads;

{$R *.dfm}

procedure TfMain.btDatasetLoopClick(Sender: TObject);
begin
  fDatasetLoop.Show;
end;

procedure TfMain.btStreamsClick(Sender: TObject);
begin
  fClienteServidor.Show;
end;

procedure TfMain.btThreadsClick(Sender: TObject);
begin
  fThreads.Show();
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
  Application.OnException := Self.OnCustomException;

  Self.FRegistroLog := TCriticalSection.Create;
end;

procedure TfMain.FormDestroy(Sender: TObject);
begin
  Application.OnException := nil;

  if Assigned(Self.FRegistroLog) then
  begin
    FreeAndNil(Self.FRegistroLog);
  end;
end;

procedure TfMain.OnCustomException(Sender: TObject; E: Exception);
var
  oLog: TStringList;
  sArquivoLog: String;
begin
  try
    Self.FRegistroLog.Enter();

    oLog := TStringList.Create;
    sArquivoLog := '.\log\log.txt';

    if FileExists(sArquivoLog) then
    begin
      oLog.LoadFromFile(sArquivoLog);
    end;

    if not DirectoryExists('.\log\') then
    begin
      CreateDir('.\log\');
    end;

    oLog.Insert(0, FormatDateTime('dd/MM/yyyy hh:mm:ss', Now) + ': ' + E.Message);

    oLog.SaveToFile(sArquivoLog);
  finally
    Self.FRegistroLog.Release();

    if Assigned(oLog) then
    begin
      FreeAndNil(oLog);
    end;

    raise E;
  end;
end;

end.
