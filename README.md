# AppManutencao

Projeto destinado à avaliação de contratação para equipe de Sustentação - Softplan. Siga as instruções deste README para realizar as correções e implementações necessárias.

Versão utilizada do Delphi: Tokyo 10.2. Pode ser utilizada outras versões (inclusive Community), desde que o código seja compatível. 

Dica: antes de iniciar as alterações, leia todo o README. 

## Instruções

### Correções a serem realizadas

- Implemente as correções e implementações descritas nas sessões abaixo.
- **Todos** os `hints` e `warnings` do projeto devem ser resolvidos. Dica: sempre rode o build (Shift + F9), ao invés do compile (Ctrl + F9), para ver todos os hints e warnings. 
Hints e Warnings

[dcc32 Warning] ClienteServidor.pas(57): W1057 Implicit string cast from 'AnsiString' to 'string'
Problema:
Isso ocorre porque a variável do tipo AnsiString só trabalha com o range de 255 caracteres da tabela ASCII.
Solução:
Alterei "FPath", na linha 28, de "AnsiString" para "String";

[dcc32 Warning] ClienteServidor.pas(78): W1057 Implicit string cast from 'AnsiString' to 'string'
Problema:
Isso ocorre porque a variável do tipo AnsiString só trabalha com o range de 255 caracteres da tabela ASCII.
Solução:
Alterei "FPath", na linha 28, de "AnsiString" para "String";

[dcc32 Warning] ClienteServidor.pas(88): W1002 Symbol 'IncludeTrailingBackslash' is specific to a platform
Solução #1 
FPath := ExtractFilePath(Application.ExeName) + 'pdf.pdf';

Solução #2
FPath := IncludeTrailingPathDelimiter(Application.ExeName) + 'pdf.pdf';

Solução #3
FPath := '.\pdf.pdf';

Solução #4
{$WARN SYMBOL_PLATFORM OFF}
FPath := IncludeTrailingBackslash(ExtractFilePath(ParamStr(0))) + 'pdf.pdf';
{$WARN SYMBOL_PLATFORM ON}
Optei por essa para manter o projeto original.

[dcc32 Warning] ClienteServidor.pas(88): W1058 Implicit string cast with potential data loss from 'string' to 'AnsiString'
Problema:
Isso ocorre porque a variável do tipo AnsiString só trabalha com o range de 255 caracteres da tabela ASCII.
Solução:
Alterei "FPath", na linha 12, de "AnsiString" para "String";

[dcc32 Warning] ClienteServidor.pas(103): W1058 Implicit string cast with potential data loss from 'string' to 'AnsiString'
Problema:
Isso ocorre porque a variável do tipo AnsiString só trabalha com o range de 255 caracteres da tabela ASCII.
Solução:
Alterei "FPath", na linha 12, de "AnsiString" para "String";

[dcc32 Warning] ClienteServidor.pas(124): W1057 Implicit string cast from 'AnsiString' to 'string'
Problema:
Isso ocorre porque a variável do tipo AnsiString só trabalha com o range de 255 caracteres da tabela ASCII.
Solução:
Alterei "FPath", na linha 12, de "AnsiString" para "String";

[dcc32 Hint] ClienteServidor.pas(134): H2077 Value assigned to 'TServidor.SalvarArquivos' never used
Problema:
"Resulta := False;" estava definido no except sem utilização alguma do valor.	
Solução:
Em "TServidor.SalvarArquivos", removido "Result := False;"

[dcc32 Warning] ClienteServidor.pas(117): W1035 Return value of function 'TServidor.SalvarArquivos' might be undefined
Problema:
Todo o método estava implementado dentro do bloco try sem retorno fora inicialmente.

Solução #1
Alterar  "Exit;" para " Exit(False);".

Solução #2
Adicionado "Result := False;" no início do método "TServidor.SalvarArquivos". Solução #1 não optei pois na região estava registrado que não poderia ser alterado.

- **Todos** os `memory leaks` do projeto devem ser resolvidos. 
Solução:
- No arquivo Foo.dproj adicionado "ReportMemoryLeaksOnShutdown := True;" para exibir os memory leaks.
- No método "procedure TfClienteServidor.FormCreate(Sender: TObject);" é instanciado "FServidor".
- Adicionado:

"procedure TfClienteServidor.FormDestroy(Sender: TObject);
begin
  if Assigned(Self.FServidor) then
  begin
    FreeAndNil(Self.FServidor);
  end;
end;"


### Como submeter uma correção 

 - Realize as implementações e correções descritas abaixo e nos envie o projeto de alguma forma: compactado por e-mail, link de algum repo (github, gitlab, etc.), ou faça upload para nuvem e nos envie o link (com acesso público).
 - Envie o projeto limpo, apenas com os **mesmos arquivos enviados originalmente** (sem dcu, binário, etc.).

## Implementações

`Implementação 1: implemente um gerenciador de exceções no projeto. A cada exceção gerada, independente do ponto da aplicação que ocorra, deve passar pelo gerenciador, e a classe e mensagem da exceção devem ser salvas em um arquivo de log. Depois de salva a mensagem no log, a exceção deve ser levantada normalmente. Fica a critério do candidato se quiser criar uma tela específica para mostrar a exceção, ou se quiser tratar algumas exceções. Isso é opcional.`

Solução:
- Adicionado método TfMain.OnCustomException;
- Adicionado "FRegistroLog: TCriticalSection;". Utilizei devido poder existir erros no paralelismo e método do log tentar escrever simultaneamente o log (arquivo), ocorrendo exceção no próprio registro do log. É o que chamamos de técnica de semáforo no windows.
- Adicionado "procedure TfMain.FormCreate(Sender: TObject);"., associando "Application.OnException" ao método OnCustomException.
- Adicionado "procedure TfMain.FormDestroy(Sender: TObject);", desassociando método "OnCustomException" de "Application.OnException".

`Implementação 2: na tela ClienteServidor, implemente o comportamento da barra de progresso para mostrar o progresso da operação de envio de arquivos.`

Solução:
Implementado.

`Implementação 3 (opcional): na tela ClienteServidor, implemente o comportamento do botão "Enviar paralelo". Esse botão deve fazer a mesma coisa que o botão "Enviar sem erros", porém de forma paralelizada, com objetivo de ganhar performance. Pense em uma solução que irá funcionar mesmo que a constante QTD_ARQUIVOS_ENVIAR tivesse valores mais altos, como mil ou dez mil`
Solução:
- No método "TfClienteServidor.btEnviarParaleloClick" implementado para rodar os processos em paralelo.

`Implementação 4: para demonstrar seu domínio em programação com Threads no Delphi, crie um formulário com o nome da unit “Threads.pas” e nome do form “fThreads”, adicione ao formulário 2 edits, um botão, uma barra de progresso e um memo (TMemo). O primeiro edit será utilizado para especificar o número de threads a serem criadas, o segundo edit para informar um valor de tempo em milissegundos máximo de espera entre cada iteração dos threads. Estes threads irão realizar um laço de 0 até 100, onde a cada iteração do laço elas deverão aguardar um tempo randômico em milissegundos, sendo o valor máximo determinado pelo usuário considerando o dado inserido no formulário. Ao iniciar o processamento um thread deve inserir no memo (TMemo) seu thread-id e o texto “Iniciando processamento" (Ex. 1543 – Iniciando processamento) e ao término do mesmo seu thread-id e o texto “Processamento finalizado" (Ex. 1543 – Processamento finalizado), os textos respectivos devem ser disparados dentro do thread em si. A cada iteração do laço, a thread deverá incrementar o valor do contador de iterações do loop na barra de progresso disponível no formulário. Todos os threads compartilharão a mesma barra de progresso, sendo então o valor mínimo da barra de progresso 0 e seu valor máximo o número de threads * vezes número de iterações (Em nosso exemplo de 0 a 100 ocorrem 101 iterações). Importante: Preocupe-se com o fechamento do formulário, o mesmo deve solicitar que as threads finalizem "gentilmente" ou esperar o término do processamento. Dica: utilize o procedimento Sleep(Milisegundos: Integer) para fazer a espera entre cada iteração do loop das threads e a função Random para garantir um valor aleatório de espera.`
Solução:
Criado fThreads.pas.

## Correções

Corrija cada defeito descrito abaixo. Na descrição do defeito terá o problema e o objetivo da correção. Para cada defeito, preencher o campo "Solução" detalhando tecnicamente a causa do problema e a solução dada. 

`Defeito 1: na tela DatasetLoop, ao clicar no botão "Deletar pares" não deleta todos os pares do dataset. Objetivo: que todos os números pares sejam deletados`
Solução:

O problema ocorria devido após o delete existir um next, podendo pular 1 registro sem verificar se o mesmo é par.

Alterado de:
if ClientDataSet.FieldByName('Field2').AsInteger mod 2 = 0 then
  ClientDataSet.Delete;

ClientDataSet.Next;"

Para:

"if ClientDataSet.FieldByName('Field2').AsInteger mod 2 = 0 then
  ClientDataSet.Delete
else  
  ClientDataSet.Next;"


`Defeito 2: na tela ClienteServidor, ocorre erro "Out of Memory" ao clicar no botão "Enviar sem erros". Objetivo: que não ocorra erro por falta de memória, e que todos os arquivos sejam enviados para a pasta Servidor normalmente.`

Solução:
- O problema ocorre devido uma limitação de memória para clientdataset. Em versões anteriores isso era resolvido alterado parâmetros na BDE.

- Na criação do formulário adicionei verificação se pasta ".\servidor" existe pois é utilizado para salvar cada arquivo.

- Alterado método "FServidor.SalvarArquivos" para corrigir problema do cds criado e não destruído ocorrendo erro de memória insuficiente.
Também adicionado parâmetro AIncremento onde a cada chamada eu passo um clientdataset com apenas 1 registro e o mesmo gera o nome do arquivo correto conforme AIncremento. E este recurso será utilizado também na "Implementação 4".

`Defeito 3: na tela ClienteServidor, ao clicar no botão "Enviar com erros", os arquivos enviados anteriormente não são apagados da pasta Servidor. Objetivo: quando ocorrer erro na operação, que é o caso que esse botão simula, os arquivos copiados anteriormente devem ser apagados, simulando um "rollback". Ou seja, no fim da operação, os arquivos devem continuar na pasta apenas se não ocorreu erro na operação. obs: não é para ser corrigido o erro que ocorre ao clicar nesse botão, visto que ele serve justamente para simular um erro.`

Solução:
- Adicionado "TfClienteServidor.btEnviarComErrosClick" barra de progresso apenas para recurso estético e poder acompanhar progresso na simulação do erro.
- Adicionado método "DeletarArquivos" na classe TServidor. É passado o parâmetro "APosicao: Integer" onde é deletado os arquivos já existente até a posição passada como parâmetro.
- Ao final da chamada alterado "FServidor.SalvarArquivos(cds.Data);" para "Self.btEnviarSemErros.Click();". Fiz isso principalmente devido a correção do "Defeito 2".
- Adicionei a mensagem "ShowMessage('Erro ao gerar arquivos!' + sLineBreak + 'Todos os arquivos gerados até o momento serão excluídos.');" apenas para existir uma pausa e usuário poder validar se realmente arquivos já gerados até o momento foram excluídos.